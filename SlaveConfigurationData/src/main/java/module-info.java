module data.SlaveConfigurationData {
	exports descriptor;
	exports descriptor.config.services;
	exports descriptor.system.view;
	exports descriptor.messages;
	requires com.fasterxml.jackson.annotation;
	requires com.fasterxml.jackson.core;
}