package descriptor.messages;


import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

import descriptor.SettingsDescriptor;


public class SlaveConfigurationInfoMsg extends SettingsDescriptor {
	
	private static final long serialVersionUID = 1L;

	public static class Value {
		public static class CardIO {
			public String id;
			public int type;
			public int slot;
			public int io;
			
			public CardIO(String id, int type, int slot, int io) {
				this.id = id;
				this.type = type;
				this.slot = slot;
				this.io = io;
			}
			public CardIO() {
				this("",-1,-1,-1);
			}
		}
		public String name;
		public long shaMsb;
        public long shaLsb;
		public ArrayList<CardIO> cards;
		
		@JsonIgnore
		public long getSHA() {
			return ((long)shaMsb << 32) | shaLsb;
		}
		
		
		public Value(String name, ArrayList<CardIO> cards, int shaMsb, int shaLsb) {
			this.name = name;
			this.cards = cards;
			this.shaLsb = shaLsb;
			this.shaMsb = shaMsb;
		}
		
		public Value(String name, ArrayList<CardIO> cards) {
			this(name,cards,0,0);
		}
		
		public Value() {
			this("",new ArrayList<CardIO>());
		}
	}
	
	public int service;
	public String topic;
	public ArrayList<Value> values;
	
	public SlaveConfigurationInfoMsg(int service,String topic,ArrayList<Value> values) {
		this.service = service;
		this.topic = topic;
		this.values = values;
	}
	
	public SlaveConfigurationInfoMsg() {
		this(-1,"",new ArrayList<Value>());
	}

	
}
