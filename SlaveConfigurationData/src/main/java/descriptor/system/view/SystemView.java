package descriptor.system.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import descriptor.SettingsDescriptor;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;



@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "cardLayout", "services", "slaves" })
public class SystemView extends SettingsDescriptor  implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	private List<SlaveDeviceLayout> slavesTopology;

	
	@Override 
	public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SystemView)) {
            return false;
        }
        SystemView obj = (SystemView)o;
		return this.slavesTopology.equals(obj.slavesTopology);
	}
	
	public SystemView() {
		this.slavesTopology = new ArrayList<SlaveDeviceLayout>();
	}
	
	public List<SlaveDeviceLayout> getSlavesTopology() {
		return Collections.unmodifiableList(slavesTopology);
	}
	public SlaveDeviceLayout getSlaveLayout(String id) {
		return slavesTopology.stream().filter( x -> x.getSlaveId().equals(id)).findFirst().orElse(null);
	}
	public SystemView addSlaveLayout(SlaveDeviceLayout slaveDeviceLayout) {
		slavesTopology.add(slaveDeviceLayout);
		return this;
	}
	
	@Override
	public String toString() {
		if(slavesTopology == null) return "";
		return String.format("slavesTopology:%s\n",
				slavesTopology.stream()
				.map(SlaveDeviceLayout::toString )
				.collect(StringBuilder::new,
						(strBuild,ioObjStr) -> { strBuild.append(ioObjStr).append(","); },
						(strBuild1,strBuild2) -> {}).toString()
				);
	}
	
}
