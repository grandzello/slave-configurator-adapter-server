package descriptor;

import java.io.Serializable;

public class SettingsDescriptor implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private String jsonRepresentation;
	
	
	public SettingsDescriptor setJsonRepresentation(String text) {
		if(text != null) {
			jsonRepresentation = new String(text);
		}
		return this;
	}
	
	public String getJsonRepresentation() {
		if(jsonRepresentation == null) return null;
		return new String(jsonRepresentation);
	}
}