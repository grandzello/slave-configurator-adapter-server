package descriptor.config.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;



@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "slaveId", "services" })
public class SlaveConfigurationDescriptor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String slaveId;
	private List<ServiceConfigurationDescriptor> services;
	
	@Override 
	public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof SlaveConfigurationDescriptor)) {
            return false;
        }
        SlaveConfigurationDescriptor obj = (SlaveConfigurationDescriptor)o;
		boolean equalLists = (this.services.size() == obj.services.size()) && (this.services.containsAll(obj.services));
		if ( (slaveId.equals(obj.slaveId)) && equalLists) {
			return true;
		}
		return false;
	}
	
	
	public SlaveConfigurationDescriptor() {
		this.services = new ArrayList<ServiceConfigurationDescriptor>();
		this.slaveId = "-";
	}
	
	public String getSlaveId() {
		return String.copyValueOf(slaveId.toCharArray());
	}
	
	
	public SlaveConfigurationDescriptor setSlaveId(String slvId) {
		this.slaveId = slvId;
		return this;
	}
	
	
	public SlaveConfigurationDescriptor addService(ServiceConfigurationDescriptor service) {
		this.services.add(service);
		return this;
	}
	
	
	public List<ServiceConfigurationDescriptor> getServices() {
		return Collections.unmodifiableList(services);
	}
	
	
	public ServiceConfigurationDescriptor findService(int serviceId, String topic) {

		return services.stream().filter(s -> {
			return (s.getServiceId() == serviceId) && (s.getTopic().equals(topic));
		}).findFirst().orElse(null);
	}
	
	@Override
	public String toString() {
		if(slaveId == null || services == null) return "";
		return String.format("slaveId: %s | services:\n%s\n",
				slaveId,
				services.stream()
				.map(ServiceConfigurationDescriptor::toString)
				.collect(StringBuilder::new,
						(strBuild,ioObjStr) -> { strBuild.append(ioObjStr); },
						(strBuild1,strBuild2) -> {}).toString());
	}
}

