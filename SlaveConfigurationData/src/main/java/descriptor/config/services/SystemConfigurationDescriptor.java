package descriptor.config.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import descriptor.SettingsDescriptor;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;


@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "config" })
public class SystemConfigurationDescriptor extends SettingsDescriptor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<SlaveConfigurationDescriptor> config;
	
	
	@Override 
	public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof SystemConfigurationDescriptor)) {
            return false;
        }
        SystemConfigurationDescriptor obj = (SystemConfigurationDescriptor)o;
		return this.config.equals(obj.config);
	}
	
	public SystemConfigurationDescriptor() {
		this.config = new ArrayList<>();
	}
	
	@JsonIgnore
	public List<SlaveConfigurationDescriptor> getSystemConfig() {
		return Collections.unmodifiableList(config);
	}
	
	public SlaveConfigurationDescriptor getSlaveConfig(String slaveId) {
		return config.stream().filter( slv -> slv.getSlaveId().equals(slaveId)).findFirst().orElse(null);
	}
	
	public SystemConfigurationDescriptor updateSlaveConfig(SlaveConfigurationDescriptor newConfig) {
		var existingConfig = getSlaveConfig(newConfig.getSlaveId());
		if(existingConfig != null) {
			int index = config.indexOf(existingConfig);
			if(index >= 0) {
				config.set(index, newConfig);
			}
		}
		else {
			addSlave(newConfig);
		}
		return this;
	}
	
	public SystemConfigurationDescriptor addSlave(SlaveConfigurationDescriptor slave) {
		this.config.add(slave);
		return this;
	}
	
	
	@Override
	public String toString() {
		if(config == null) return "";
		return String.format("config:\n%s\n",
				config.stream()
				.map(SlaveConfigurationDescriptor::toString)
				.collect(StringBuilder::new,
						(strBuild,ioObjStr) -> { strBuild.append(ioObjStr); },
						(strBuild1,strBuild2) -> {}).toString());
	}
	
	
}
