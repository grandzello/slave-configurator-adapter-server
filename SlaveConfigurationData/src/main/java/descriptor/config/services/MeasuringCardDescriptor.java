
package descriptor.config.services;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "type", "id", "state", "ioLabels" })
public class MeasuringCardDescriptor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int type;
	private int state;
	private String id;
	private List<String> ioLabels;
	
	
	@Override 
	public boolean equals(Object o) {

        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof MeasuringCardDescriptor)) {
            return false;
        }
        MeasuringCardDescriptor obj = (MeasuringCardDescriptor)o;
		boolean equalLists = (this.ioLabels.size() == obj.ioLabels.size()) && (this.ioLabels.containsAll(obj.ioLabels));
		if ((this.type == obj.type) && (this.state == obj.state) && (this.id.equals(obj.id)) && equalLists) {
			return true;
		}
		return false;
	}
	
	public MeasuringCardDescriptor() {
		this.type = -1;
		this.state = 1;
		this.id = "";
	}
	
	public MeasuringCardDescriptor setType(int type) {
		this.type = type;
		return this;
	}
	public MeasuringCardDescriptor setState(int state) {
		this.state = state;
		return this;
	}
	public MeasuringCardDescriptor setId(String serial) {
		this.id = serial;
		return this;
	}
	
	public MeasuringCardDescriptor setIoLabels(List<String> ioLabels) {
		this.ioLabels = ioLabels;
		return this;
	}
	
	public List<String> getIoLabels() {
		return ioLabels;
	}
	
	public int getType() { return type; }
	public int getState() { return state; }
	public String getId() { return id; }
	
	@Override
	public String toString() {
		if(id == null) return "";
		return String.format("type: %d | id: %s | state: %d | ioLabels: %s\n",
				type,id,state,ioLabels);
	}
	
}