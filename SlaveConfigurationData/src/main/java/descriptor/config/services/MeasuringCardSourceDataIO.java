package descriptor.config.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
/*
 
 {
					"cardId": "1234",
					"cardType": 0,
					"ioNum": [1, 3]
				}
 */

//Jezeli brak @JsonIgnore przy metodzie, ktora zwraca to member poddany serializacji
//i ta metoda nazywa sie inaczej niz member, to utworzone zostanie nowe pole w json.

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonPropertyOrder({ "cardId", "cardType","cardSlot", "ioNum" })
public class MeasuringCardSourceDataIO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cardId;
	private int cardType;
	private int cardSlot;
	private List<Integer> ioNum;
	
	
	@Override 
	public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || !(o instanceof MeasuringCardSourceDataIO)) {
            return false;
        }
        MeasuringCardSourceDataIO obj = (MeasuringCardSourceDataIO)o;
		boolean equalLists = (this.ioNum.size() == obj.ioNum.size()) && (this.ioNum.containsAll(obj.ioNum));
		if (this.cardId.equals(obj.cardId) && (this.cardType == obj.cardType) && (this.cardSlot == obj.cardSlot)
				&& equalLists) {
			return true;
		}
		return false;
	}
	

	
	public MeasuringCardSourceDataIO() {
		this.ioNum = new ArrayList<Integer>();
		this.cardId = "";
		this.cardType = -1;
	}
	
	public MeasuringCardSourceDataIO(String cardId,int cardSlot, int cardType,int ... ioNum) {
		this.cardId = cardId;
		this.cardType = cardType;
		this.cardSlot = cardSlot;
		this.ioNum = new ArrayList<Integer>(Arrays.asList( IntStream.of(ioNum).boxed().toArray(Integer[]::new)) );
	}
	
	public int getCardSlot() {
		return cardSlot;
	}
	
	public String getCardId() {
		return String.copyValueOf(cardId.toCharArray());
	}
	@JsonIgnore
	public int getType() {
		return cardType;
	}
	@JsonIgnore
	public List<Integer> getIONums() {
		return Collections.unmodifiableList(ioNum);
	}
	
	@Override
	public String toString() {
		if(cardId == null || ioNum == null) return "";
		return String.format(
				"cardId:%s | cardType: %d | cardSlot: %d | ios: %s\n", 
				cardId,cardType,cardSlot,Arrays.toString(ioNum.toArray()));
	}
	

}
