
package support;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.node.ObjectNode;

import descriptor.SettingsDescriptor;





public class SlaveConfigurationUtils {
	

	static public class TopicParser {
		
		 List<String> tokens = new ArrayList<>();
		public TopicParser(String topic) {
			 tokens = Arrays.stream(topic.split("/")).collect(Collectors.toList());
		}
		
		public String getSlaveId() {
			return getNode(tokens.size() - 1);
		}
		public String getNode(int index) {
			if(!tokens.isEmpty()) {
				return tokens.get(index);
			}
			return null;
		}
	}
	
	public static Map<String, ImmutablePair<WebResourceType, byte[]>> webResourceCache;

	static {
		webResourceCache = new TreeMap<>();
	}
	
	
	
	public static WebResourceType getWebResourceTypeLabel(String resourceName) {
		var ext = FilenameUtils.getExtension(resourceName);
		
		if(ext.equals("png")) return WebResourceType.IMAGE_PNG;
		else if(ext.equals("gif")) return WebResourceType.IMAGE_GIF;
		else if(ext.equals("jpg")) return WebResourceType.IMAGE_JPG;
		else if( ext.equals("css") ) return WebResourceType.CSS;
		else if( ext.equals("html") ) return WebResourceType.HTML;
		else if (ext.equals("js") ) return WebResourceType.JAVA_SCRIPT;
		else if( ext.equals("json")) return WebResourceType.JSON;
 
		return WebResourceType.UNKNOWN;
	}
	
	
	public static ImmutablePair<WebResourceType, byte[]> findWebResourceAndCacheIt(URL dir, String resourceName) throws Exception {
		ImmutablePair<WebResourceType, byte[]> data = null;
		data = webResourceCache.get(resourceName);
		if(data != null) return data;
		
		File resource = Files.walk(Path.of(dir.toURI()))
				.filter(path -> {
					/*
					System.out.println(Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS) + "|"
					+ path.getFileName().toString() + "|"
					+ resourceName);
					*/
					return 
						Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS)
						&& path.getFileName().toString().equals(resourceName); } )
				.map(Path::toFile ).findFirst().orElseThrow();
		
		
		data = new ImmutablePair<>(getWebResourceTypeLabel(resourceName),Files.readAllBytes(resource.toPath()));
		webResourceCache.put(resource.getName(),data);
			
		return data;
	}
	
	public static String createStatusResponse(ConfigStatus status) {

		 ObjectMapper mapper = new ObjectMapper();
		 var statusMsg = mapper.createObjectNode().put("configStatus", status.name());
		 try {
			 return mapper.writeValueAsString(statusMsg);
		 }
		 catch(JsonProcessingException e) {
			 e.printStackTrace();
		 }
		 return null;
	
	 }
	
	/*
	public static <T extends SettingsDescriptor> SettingsDescriptor jsonToSettingsObject(String json, Class<? extends SettingsDescriptor> c) throws Exception  {
	
		ObjectMapper jsonMapper = new ObjectMapper(); 
		return ((SettingsDescriptor)jsonMapper.readValue(json,c)).setJsonRepresentation(json);
	}
	
	public static String settingsObjectToJson(Object o) throws Exception {
		ObjectMapper jsonMapper = new ObjectMapper(); 
		return  jsonMapper.writeValueAsString(o);
	}
	*/
	
	public static SettingsDescriptor jsonToSettingsObject(String json, Class<? extends SettingsDescriptor> c)  {
		try {
		ObjectMapper jsonMapper = new ObjectMapper(); 
		return ((SettingsDescriptor)jsonMapper.readValue(json,c)).setJsonRepresentation(json);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	static private class IgnoreInheritedIntrospector extends JacksonAnnotationIntrospector {
	
		private static final long serialVersionUID = 1L;

		@Override
		public boolean hasIgnoreMarker(final AnnotatedMember m) {
			return m.getDeclaringClass() == SettingsDescriptor.class || super.hasIgnoreMarker(m);
		}
	}

	public static String settingsObjectToJson(SettingsDescriptor o) {
		ObjectMapper jsonMapper = new ObjectMapper(); 
		jsonMapper.setAnnotationIntrospector(new IgnoreInheritedIntrospector());
		String jsonText = null;
		try {
			jsonText = jsonMapper.writeValueAsString(o);
			o.setJsonRepresentation(jsonText);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return jsonText;
	}
	
	
	
	
	
}