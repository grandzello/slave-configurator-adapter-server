package support;

public enum WebResourceType {
	UNKNOWN(""),
	IMAGE_PNG("image/png"),
	IMAGE_JPG("image/jpeg"),
	IMAGE_GIF("image/gif"),
	HTML("text/html"),
	CSS("text/css"),
	JSON("application/json"),
	JAVA_SCRIPT("text/javascript");
	//MAP("text/css");
	
	private  WebResourceType(String type) {
		this.type = type;
	}
	
	private String type;
	
	@Override
	public String toString() {
		return type;
	}
}
