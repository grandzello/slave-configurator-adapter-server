package adapter.slave.config;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import descriptor.config.services.MeasuringCardSourceDataIO;
import descriptor.config.services.ServiceConfigurationDescriptor;
import descriptor.config.services.ServiceTaskDescriptor;
import descriptor.config.services.SlaveConfigurationDescriptor;
import descriptor.config.services.SystemConfigurationDescriptor;
import descriptor.messages.SlaveConfigurationInfoMsg;
import descriptor.system.view.SlaveDeviceLayout;
import descriptor.system.view.SystemView;
import support.SlaveConfigurationUtils;






public class AdapterHostBridge {

	private final static String hostViewTopic = "state/slave/host-view/";		//do publikowania/odbioru info na temat topologii slave
	private final static String hostConfigTopic = "state/slave/host-config/"; //do pubikowania nowej konfiguracji utworzonej przez usera
	
	public static final String CONTEXT_NAME = "adapterHostBridge";
	
	
	private static AdapterHostBridge instance;
	private MqttAsyncClient mqttClient;
	private SystemSettingsManager sysManager;
	
	private AdapterHostBridge() {
		sysManager = new SystemSettingsManager();
	}
	
	private void sendJsonMsg(String topic,String jsonMsg) {
		if(!mqttClient.isConnected()) return;
		MqttMessage msg = new  MqttMessage(jsonMsg.getBytes());
	    msg.setQos(2);
	    try {
			mqttClient.publish(topic,  msg );
		} catch (MqttException e) {
			e.printStackTrace();
		}	
	}
	
	
	public SystemSettingsManager getSystemSettingsManager() {
		return sysManager;
	}
	

	private void onReceiveJsonMsg(String topic,IMqttMessageListener listener) {
		try {
			if (mqttClient != null) {
				mqttClient.subscribe(topic, 2,listener);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static AdapterHostBridge getInstance() {
		if (instance == null) {
			synchronized (AdapterHostBridge.class) {
				if (instance == null) {
					instance = new AdapterHostBridge();
				}
			}
		}
		return instance;
	}
	

	public void sendSystemConfigurationToHost() {
		var sysConfig = sysManager.getConfigurationCopy();
		
		if(sysConfig != null) {
			
			Map<String, ArrayList<SlaveConfigurationInfoMsg>> messagesMap = sysConfig.getSystemConfig()
					.stream()
					.filter(slvConf -> sysManager.getUpdateConfigSlaveRegister().contains(slvConf.getSlaveId()))
					.collect(Collectors.groupingBy(SlaveConfigurationDescriptor::getSlaveId,
							Collector.of(ArrayList::new, (listOfMessages, slvCnf) -> {
								
								if(slvCnf.getServices().isEmpty()) {
									//gdy konfiguracja slave w wyniku zmian uzytkownika stala sie pusta to i tak wyslij specjalny message serwisu ze znacznikiem -1
									listOfMessages.add(new SlaveConfigurationInfoMsg());
								}
								else {
								List<SlaveConfigurationInfoMsg> messages = slvCnf.getServices().stream().map(service -> {
									ArrayList<SlaveConfigurationInfoMsg.Value> vs = service.getTasks().stream()
											.collect(ArrayList::new, (list, task) -> {
												SlaveConfigurationInfoMsg.Value v = task.getIOs().stream().map(io -> {
													ArrayList<SlaveConfigurationInfoMsg.Value.CardIO> cardIOs = io
															.getIONums().stream()
															.collect(ArrayList::new, (listOfCardIo, ioObj) -> {
																listOfCardIo.add(new SlaveConfigurationInfoMsg.Value.CardIO(
																		io.getCardId(), io.getType(),io.getCardSlot(),ioObj.intValue()));
															}, (l1, l2) -> {
															});
													return new SlaveConfigurationInfoMsg.Value(task.getName(), cardIOs);
												}).findFirst().get();

												list.add(v);
											}, (l1, l2) -> {
											});

									return new SlaveConfigurationInfoMsg(service.getServiceId(), service.getTopic(), vs);

								}).collect(Collectors.toList());

								listOfMessages.addAll(messages);
								}

							}, (l1, l2) -> {
								return l1;
							}, Collector.Characteristics.UNORDERED)));
			
			
			
			messagesMap.forEach( (slaveId, messages) -> {
				messages.forEach( message -> {
					sendJsonMsg(hostConfigTopic + slaveId,SlaveConfigurationUtils.settingsObjectToJson(message));
				});
			});
			
		
		}
	}
	
	
	public void sendSystemViewToHost() {
		
		SystemView sysView = sysManager.getSystemViewCopy();
		
		sysView.getSlavesTopology().stream()
				.filter(slvView -> sysManager.getUpdateViewSlaveRegister().contains(slvView.getSlaveId()))
				.forEach(slvView -> {
					sendJsonMsg(hostViewTopic + slvView.getSlaveId(),
							SlaveConfigurationUtils.settingsObjectToJson(slvView));
				});
	}
	
	
	

	
	public void listenForConfigurationUpdateFromHostService() {

		onReceiveJsonMsg(hostConfigTopic + "#", (topic, message) -> {
			try {
				System.out.println("host ----> ui");
				System.out.println("on topic:" + topic + "\n" + message);

				SlaveConfigurationInfoMsg slaveConfigurationInfoMsg = (SlaveConfigurationInfoMsg) SlaveConfigurationUtils
						.jsonToSettingsObject(message.toString(), SlaveConfigurationInfoMsg.class);

				SystemConfigurationDescriptor sysConfig = sysManager.getConfiguration();
				

				String slaveId = new SlaveConfigurationUtils.TopicParser(topic).getSlaveId();

				
				SlaveConfigurationDescriptor slaveConfiguration = sysConfig.getSlaveConfig(slaveId);
				
		
				ServiceConfigurationDescriptor service = null;
				
				if (slaveConfiguration == null) {
					slaveConfiguration = new SlaveConfigurationDescriptor().setSlaveId(slaveId);
					sysConfig.addSlave(slaveConfiguration);
				}

				// Referencja service bedzie wskazywac na istniejacy obiekt service lub nowo
				// utworzony.
				// W ten sposob za posrednictwiem jednej referencji service mozna aktualizowac
				// lub dodac nowy serwis bez tworzenia duplikatow
				
				service = slaveConfiguration.findService(slaveConfigurationInfoMsg.service,
						slaveConfigurationInfoMsg.topic);

				if (service == null) {
					service = new ServiceConfigurationDescriptor();
					slaveConfiguration.addService(service);
				}

				List<MeasuringCardSourceDataIO> ios = slaveConfigurationInfoMsg.values.stream()
						.flatMap(v -> v.cards.stream())
						.collect(Collectors.groupingBy(io -> io.id,
								Collectors.mapping(io -> Triple.of(io.slot,io.io, io.type),
										Collectors.toList())))
						.entrySet().stream().collect(ArrayList::new, (list, entry) -> {
							//type jest dla danej karty taki sam dlatego z listy pobieramy pierwszy element i wyciagamy type: entry.getValue().get(0)
							list.add(new MeasuringCardSourceDataIO(entry.getKey(), //cardId
									entry.getValue().get(0).getLeft(), //card slot
									entry.getValue().get(0).getRight(),//card type 
									entry.getValue().stream().mapToInt(Triple::getMiddle).toArray())); //card Io
						}, (c1, c2) -> {
						});
				
				var newTasks = slaveConfigurationInfoMsg.values.stream()
						.map(task -> new ServiceTaskDescriptor(task.name, ios)).collect(Collectors.toList());

				// na tym topicu dane sa wysylane jak i odbierane. W przypadku wysyalania ten if
				// zatrzymuje przetwarzanie na odbiorze message, ktory przed chwila zostal wysylany
				if ((service.getServiceId() == slaveConfigurationInfoMsg.service)
						&& (service.getTopic().equals(slaveConfigurationInfoMsg.topic))
						&& service.getTasks().equals(newTasks)) {
					return;
				}

				service.setServiceId(slaveConfigurationInfoMsg.service).setTopic(slaveConfigurationInfoMsg.topic)
						.setTasks(newTasks);

		
				String jsonText = SlaveConfigurationUtils.settingsObjectToJson(sysConfig);
				System.out.println("SYS CONFIG:");
				System.out.println(jsonText);
				
				
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		});

	}

	public void listenForSystemViewUpdateFromHostService() {

		onReceiveJsonMsg(hostViewTopic + "#", (topic, message) -> {

			try {
				System.out.println("host ----> ui");
				System.out.println("on topic:" + topic + "\n" + message);
				
				SlaveDeviceLayout slaveViewMsg = (SlaveDeviceLayout) SlaveConfigurationUtils
						.jsonToSettingsObject(message.toString(), SlaveDeviceLayout.class);
				
				SystemView sysView = sysManager.getSystemView();
				
				String slaveId = new SlaveConfigurationUtils.TopicParser(topic).getSlaveId();
				
				SlaveDeviceLayout slaveDeviceLayout = sysView.getSlaveLayout(slaveId);
				
				//na tym topicu dane sa wysylane jak i odbierane. W przypadku wysyalania ten if zatrzymuje
				//przetwarzanie na odbiorze message, ktory przed chwila zostal wysylany
				if(slaveViewMsg.equals(slaveDeviceLayout)) {
					return;
				}
				
				if(slaveDeviceLayout == null) {
					//dodanie nowej topologii slave'a
					sysView.addSlaveLayout(slaveViewMsg);
					slaveDeviceLayout = slaveViewMsg;
				}
				else {
					//aktualizacja poprzednich ustawien
					slaveDeviceLayout = slaveViewMsg;
				}
				
			
				String jsonText = SlaveConfigurationUtils.settingsObjectToJson(sysView);
				System.out.println("SYS VIEW:");
				System.out.println(jsonText);

			} catch (Exception e) {
				e.printStackTrace();
			}
		});

}

	
	
	public boolean init(String mqttBrokerAddress) {
		boolean result = true;
		try {
	
			String mqttClientId = UUID.randomUUID().toString();
			mqttClient = new MqttAsyncClient(mqttBrokerAddress,mqttClientId);

			System.out.println("Publisher created: <id:" + mqttClientId + ">");

			MqttConnectOptions options = new MqttConnectOptions();
			options.setAutomaticReconnect(true);
			options.setCleanSession(true);
			options.setConnectionTimeout(3);

			System.out.println("Trying connect to broker...");

			mqttClient.connect(options);
			 
			/*
			while(!mqttClient.isConnected()) {
		    	Thread.yield();
		    	Thread.sleep(1000);
			}
			*/

		    int count = 5;
		    while(!mqttClient.isConnected()) {
		    	Thread.yield();
		    	Thread.sleep(1000);
		        if(count == 0) {
		        	result = false;
		            break;
		        }
		        count--;
		    }
		    

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		
		return result;
	}
	
	
	public boolean deInit() {
		boolean result = true;
		if (mqttClient != null) {
			System.out.println("Close client connection!\n");
			try {
				if (mqttClient.isConnected()) {
					mqttClient.disconnectForcibly();
					mqttClient.close(true);
				}
			} catch (MqttException e) {
				e.printStackTrace();
				result = false;
			}
		}
		return result;
	}
}

