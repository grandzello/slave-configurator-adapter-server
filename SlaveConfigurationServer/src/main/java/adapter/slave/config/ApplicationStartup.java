package adapter.slave.config;



import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

public class ApplicationStartup implements ServletContextListener {
	

	

	@Override
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("initialization of application");
		
		var hostServiceBridge = AdapterHostBridge.getInstance();
		var context = event.getServletContext();
		
		String mqttBrokerAddress = context.getInitParameter("mqtt-broker-ip");
		System.out.println("mqtt broker address:" + mqttBrokerAddress);

		if (hostServiceBridge.init(mqttBrokerAddress)) {

			System.out.println("Connected to Broker!");
			
			hostServiceBridge.listenForConfigurationUpdateFromHostService();
			
			hostServiceBridge.listenForSystemViewUpdateFromHostService();
			
			context.setAttribute(AdapterHostBridge.CONTEXT_NAME, hostServiceBridge);
		}
		else {
			System.out.println("host service bridge not initialized properly!");
		}


		
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
	
		System.out.println("deinitialization of application!");
		
		var hostServiceBridge = (AdapterHostBridge)event.getServletContext().getAttribute(AdapterHostBridge.CONTEXT_NAME);
		
		if(hostServiceBridge != null) {
			if( hostServiceBridge.deInit() ) {
				System.out.println("bridge to Host Service closed properly!");
			}
		}
	}
	


}
