package adapter.slave.config;


import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntSupplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;


import org.apache.commons.lang3.SerializationUtils;

import descriptor.SettingsDescriptor;
import descriptor.config.services.MeasuringCardDescriptor;
import descriptor.config.services.SlaveConfigurationDescriptor;
import descriptor.config.services.SystemConfigurationDescriptor;
import descriptor.system.view.SlaveDeviceLayout;
import descriptor.system.view.SystemView;
import support.ConfigStatus;
import support.SlaveConfigurationUtils;

public class SystemSettingsManager {

	
	public static class Settings extends SettingsDescriptor implements Serializable {

		private static final long serialVersionUID = 1L;
		
		public SystemConfigurationDescriptor systemServices;
		public SystemView systemTopology;
		public Settings() {
			systemServices = new SystemConfigurationDescriptor();
			systemTopology = new SystemView();
		}
	}
	
	private Object latestConfigMutex;
	private Object systemViewMutex;
	private Object allSettingsMutex;
	//rejestry te przechowuja id Slaveow, ktore wymagaja aktualizacji na Hoscie,
	//po to aby nie wysylac message dla slave'ow dla ktorych nic sie nie zmienilo w ustawieniach.
	private Queue<String> updateConfigSlaveRegister;
	private Queue<String> updateViewSlaveRegister;
	Settings settings;
	
	public SystemSettingsManager() {
		latestConfigMutex = new Object();
		systemViewMutex = new Object();
		allSettingsMutex = new Object();
		settings = new Settings();
		updateConfigSlaveRegister = new LinkedList<>();
		updateViewSlaveRegister = new LinkedList<>();
	}
	
	

	boolean updateSystemConfigObject(String sysConfJson)  {
		synchronized(latestConfigMutex) {
			SystemConfigurationDescriptor tmp = (SystemConfigurationDescriptor) 
					SlaveConfigurationUtils.jsonToSettingsObject(sysConfJson,SystemConfigurationDescriptor.class );
			if(tmp != null) {
				settings.systemServices = tmp;
				return true;
			}
		}
		return false;
	}
	
	boolean updateSystemViewObject(String sysViewJson) {

		synchronized (systemViewMutex) {
			// aby nie pozostawic obiektu konfiguracji w niespojnym stanie w przypadku
			// rzucenia wyjatku - przypisz zmapowany json do nowej referencji a pozniej do
			// docelowej
			SystemView tmp = (SystemView) SlaveConfigurationUtils.jsonToSettingsObject(sysViewJson, SystemView.class);
			if (tmp != null) {
				settings.systemTopology = tmp;
				return true;
			}
		}
		return false;
	}
	

	public Queue<String> getUpdateConfigSlaveRegister() {
		return updateConfigSlaveRegister;
	}
	public Queue<String> getUpdateViewSlaveRegister() {
		return updateViewSlaveRegister;
	}
	
public List<ConfigStatus> analyzeChangeOfSettings(Settings newSettings) {
	
	
		Function<SystemView,Long> countCommitedSlaves = slvView -> slvView.getSlavesTopology().stream().filter(SlaveDeviceLayout::isCommited).count();

		List<ConfigStatus> indicators = new ArrayList<>();
		
		//zdarzenie zmiany konfiguracji - dodania/usniecia serwisow/taskow
		
		if(! settings.systemServices.equals(newSettings.systemServices)) {
			
			System.out.println("<analyzeChangeOfSettings> Updating system configuration!\n" + newSettings.systemServices);
		
			List<SlaveConfigurationDescriptor> newSystemConfigCopy = 
					new ArrayList<>(newSettings.systemServices.getSystemConfig());
			
			newSystemConfigCopy.removeAll(settings.systemServices.getSystemConfig());
			if(!newSystemConfigCopy.isEmpty()) {
				
				for(var slaveConfig : newSystemConfigCopy) {
					settings.systemServices.updateSlaveConfig(slaveConfig);
					updateConfigSlaveRegister.add(slaveConfig.getSlaveId());
				}
				indicators.add(ConfigStatus.SYSTEM_CONF_CHANGED);				
			}
		}

		//zdarzenie: nowe slavy zostal dodane przez uzytownika - zaktualizuj flage commited w system-view
		if(countCommitedSlaves.apply(settings.systemTopology) != countCommitedSlaves.apply(newSettings.systemTopology)) {
			
			System.out.println("<analyzeChangeOfSettings> Updating slave topology!");
			
			List<SlaveDeviceLayout> newSystemTopologyCopy = 
					new ArrayList<>(newSettings.systemTopology.getSlavesTopology());
			
			//jezeli okaze sie ze zmiany beda dotyczyly wszystkich elementow listy newSystemTopologyCopy, to pozotanie
			//ona nie naruszona, bo z niej usuwane sa tylko elementy, ktore sa takie same jak w oryginalenj liscie
			//dlatego weryfikowany jest size listy jako wskaznik zmian
			newSystemTopologyCopy.removeAll(settings.systemTopology.getSlavesTopology());
			if(!newSystemTopologyCopy.isEmpty()) {
				
				for(var slaveLayout : newSystemTopologyCopy) {
					settings.systemTopology.getSlaveLayout(slaveLayout.getSlaveId()).addToManagedSlaves();
					updateViewSlaveRegister.add(slaveLayout.getSlaveId());
				}
				
				indicators.add(ConfigStatus.SYSTEM_VIEW_CHANGED);
			}
		}
		
		// zdarzenie: zmiana nazw etykiet dla IO
		for (SlaveDeviceLayout slvView : newSettings.systemTopology.getSlavesTopology()) {
			var slvId = slvView.getSlaveId();
			for (MeasuringCardDescriptor cardDescriptor : slvView.getCards()) {
				var card = settings.systemTopology.getSlaveLayout(slvId).getCard(cardDescriptor.getId());
				if (card != null) {
					if (!cardDescriptor.getIoLabels().equals(card.getIoLabels())) {
						card.setIoLabels(cardDescriptor.getIoLabels());
						indicators.add(ConfigStatus.SYSTEM_VIEW_CHANGED);
						if(	!updateViewSlaveRegister.contains(slvId )) {
							updateViewSlaveRegister.add(slvId);
						}
					}
				}
			}
		}

		if(indicators.contains(ConfigStatus.SYSTEM_VIEW_CHANGED)) {
			settings.systemTopology.setJsonRepresentation( SlaveConfigurationUtils.settingsObjectToJson(settings.systemTopology) );
		}
		if(indicators.contains(ConfigStatus.SYSTEM_CONF_CHANGED)) {
			settings.systemServices.setJsonRepresentation( SlaveConfigurationUtils.settingsObjectToJson(settings.systemServices) );
		}
		
		
		return indicators;
		
	}
	


	
	SystemView getSystemViewCopy() {
		synchronized (systemViewMutex) {
			return SerializationUtils.clone(settings.systemTopology);

		}
	}
	
	
	SystemView getSystemView() {
		return settings.systemTopology;

	}
	
	
	SystemConfigurationDescriptor getConfigurationCopy() { 
		synchronized(latestConfigMutex) {
			return SerializationUtils.clone(settings.systemServices);
		}
	}
	SystemConfigurationDescriptor getConfiguration() { 
		return settings.systemServices;
	}
	
	
	public Settings getAllSettingsCopy() {
		synchronized(allSettingsMutex) {
			return SerializationUtils.clone(settings);
		}
	}

	public Settings getAllSettings() {
		return settings;
	}
}
