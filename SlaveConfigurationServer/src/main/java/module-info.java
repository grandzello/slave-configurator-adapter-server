module adapter.slave.config.SlaveConfigurationServer {
	exports adapter.slave.config;
	opens adapter.slave.config;
	requires transitive jakarta.servlet;
	requires transitive support.SlaveConfigurationUtils;
	requires transitive data.SlaveConfigurationData;
	requires transitive org.eclipse.paho.client.mqttv3;
	requires org.apache.commons.lang3;
}